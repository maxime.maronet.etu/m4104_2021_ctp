package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    private String salle;
    private String poste;
    private String DISTANCIEL;
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;
        model = new SuiviViewModel(getActivity().getApplication());
        Spinner spSalle = (Spinner)view.findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner)view.findViewById(R.id.spPoste);

        ArrayAdapter<CharSequence> adapSalle = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        adapSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapSalle);

        ArrayAdapter<CharSequence> adapPoste = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        adapPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText login = (EditText)view.findViewById(R.id.tvLogin);
            model.setUsername(login.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update(spSalle, spPoste);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        update(spSalle, spPoste);
        // TODO Q9
    }

    private void update(Spinner spSalle, Spinner spPoste) {
        if(spSalle.getSelectedItem().equals(DISTANCIEL)) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
        }
    }
    // TODO Q9
}